<?php
/**
 * This file creates the VTT file with pictures to show while playing video.
 */
session_start();							// Start the session
header ("Content-type: text/vtt");	// Vi sender svaret som vtt data

require_once 'include/db.php';
require_once 'classes/video.inc.php'; // Get video class

$id = $_GET['id'];
$video = new Video($db, $id);

echo $video->getPicVTT();