<?php
/**
 * Index.php
 * 
 * Shows:
 * 		The last video that has been uploaded
 * 		The last playlist that has been added
 *
 * 		User's last videos (if logged in)
 * 		User's playlist (if logged in)
 *
 * 
 * Source for carousel in Latest videos:
 * http://stackoverflow.com/questions/18850099/carousel-with-thumbnails-in-bootstrap-3-0 + http://www.bootply.com/81478
 */


session_start();						 // Start the session
require_once 'include/db.php'; 			 // Connect to the database
require_once 'classes/user.inc.php';	 // Get user class

$ajax = false;
if(isset($_SERVER['HTTP_X_PJAX'])) { 	 // request was sent with ajax, show only content.
  $ajax = true;
}

$user = new User($db);					 // Create user object.

$pageTitle = 'Video DB'; 				 // Finn på no :-P
if(!$ajax)
  require_once 'include/header.php';

require_once 'classes/video.inc.php'; 	 // Get Video class.
$video = new Video($db); 				 // Video object.

$last = $video->find();

if(!$ajax) {
?>
<div id="main-content">
<?php
}
if($ajax && isset($cssInc)) {
  foreach($cssInc as $c) {
    echo "<link rel=\"stylesheet\" href=\"css/$c\" />\n";
  }
}
?>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
          <h3>Latest videos</h3>
          
          <div class="well">
              <?php
              $total = count($last);
              if($total) {
              ?>
              <div id="myCarousel" class="carousel slide">
                  
                  <!-- Carousel items -->
                  <div class="carousel-inner">
                  <?php
                  $count = 0;
                  foreach($last as $vid) {
                    if(($count++ % 4) == 0) {
                      ?>
                      <div class="item<?php if($count == 1) echo ' active'; ?>">
                        <div class="row">
                      <?php
                    }
                    ?>
                          <div class="col-sm-3">
                            <div class="thumbnail">
                              <a data-pjax href="player.php?id=<?php echo $vid->id; ?>" class="thumbnail">
                                <img src="<?php echo $vid->getThumb(); ?>" alt="Image" class="img-responsive">
                                <div class="carousel-caption">
                                  <h3><?php echo $vid->getTitle(25); ?></h3>
                                  <p><?php echo $vid->getDescription(25); ?></p>
                                </div>
                              </a>
                            </div>
                          </div> 
                    <?php
                    if(($count % 4) == 0 || $count == $total) {
                      ?>
                        </div>
                      </div>
                      <?php
                    }
                  }
                  ?>
                  </div>
                  <!--/carousel-inner--> <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>

                  <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
              </div>
              <!--/myCarousel-->
              <?php
              } else {
                echo '<p>Ingen videoer.</p>';
              }
              ?>
          </div>
          <!--/well-->
      </div>
    </div>
    <?php
    require_once 'classes/playlist.inc.php'; 			 // Get playlist class
    $playlist = new Playlist($db);
    if($user->isLoggedIn()) {
      $mypls = $playlist->getPlaylistsByUser($user->uid); // Get my playlists.
    ?>
    <div class="row">
      <div class="col-md-12">
        <h3>My playlists</h3>
        <div class="list-group">
          <?php
          foreach($mypls as $pl) {
            echo '<a data-pjax href="player.php?playlist='.$pl->id.'" class="list-group-item">'.htmlspecialchars($pl->title).' <span class="badge">'.$pl->lastVideoPos().'</span></a>';
          }
          ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <h3>My Videos</h3>
        <div class="well">
              <?php
              $last = $video->findByUser($user->uid);
              $total = count($last);
              if($total) {
              ?>
              <div id="myCarousel" class="carousel slide">
                  
                  <!-- Carousel items -->
                  <div class="carousel-inner">
                  <?php
                  $count = 0;
                  foreach($last as $vid) {
                    if(($count++ % 4) == 0) {
                      ?>
                      <div class="item<?php if($count == 1) echo ' active'; ?>">
                        <div class="row">
                      <?php
                    }
                    ?>
                          <div class="col-sm-3">
                            <div class="thumbnail">
                              <a data-pjax href="player.php?id=<?php echo $vid->id; ?>" class="thumbnail">
                                <img src="<?php echo $vid->getThumb(); ?>" alt="Image" class="img-responsive">
                                <div class="carousel-caption">
                                  <h3><?php echo $vid->getTitle(25); ?></h3>
                                  <p><?php echo $vid->getDescription(25); ?></p>
                                </div>
                              </a>
                            </div>
                          </div> 
                    <?php
                    if(($count % 4) == 0 || $count == $total) {
                      ?>
                        </div>
                      </div>
                      <?php
                    }
                  }
                  ?>
                  </div>
                  <!--/carousel-inner--> <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>

                  <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
              </div>
              <!--/myCarousel-->
              <?php
              } else {
                echo '<p>Ingen videoer.</p>';
              }
              ?>
          </div>
          <!--/well-->
        
      </div>
    </div>
    <?php
    }
    $alla = $playlist->getAll();
    if(count($alla)) {
    ?>
    <div class="row">
      <div class="col-md-12">
        <h3>Latest playlists</h3>
        
        <table id="user-table" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Id</th>
              <th>Title</th>
              <th>Num videos</th>
              <th>UserID</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach($alla as $pl) {
              echo '<tr class="clickable-row" data-href="player.php?playlist='.$pl->id.'"><td>'.$pl->id.'</td><td>'.$pl->title.'</td><td>'.$pl->lastVideoPos().'</td><td>'.$pl->userid.'</td></tr>';
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
    <?php
    }
    ?>
  </div>
<!-- </div> (Skip last end div, this is in footer.php -->

<?php
$jsIncInline = array('
jQuery(document).ready(function($) {
    $(".clickable-row").css("cursor", "pointer");
    $(".clickable-row").click(function() {
        //window.location = $(this).data("href");
        $.pjax({url: $(this).data("href"), container: "#main-content"});
    });
});
$("#user-table").DataTable();
');
require_once 'include/footer.php';
