/**
 * In this file pjax get initialized and the search form (in the header) 
 */

$(document).ready( function () {
  $(document).pjax('a[data-pjax]', '#main-content'); // Initialize pjax to insert the contents to main-content DIV
  $.pjax.defaults.timeout = 2000; // Sett the default timeout to 2 seconds.
  
  $(document).on('pjax:error', function(xhr, textStatus, error, options) { // On error
    console.log(error);
  });
  
  $(document).on('pjax:send', function() { // Show loading spinner when pjax is making a request.
    $('.loader').show()
  });
  $(document).on('pjax:complete', function() { // Hide loading spinner when done.
    $('.loader').hide()
  });
  
  $('#search-form').submit(function(e) { // Do search with pjax
    e.preventDefault();
    $.pjax.submit(e, "#main-content");
  });
});