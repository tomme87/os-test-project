
/**
 * AdminLanguages.js
 * 
 * Changes the language tabel i adminLanguages.php when nye languages are added.
 * 
 * Gives feedback to the user.
 * Tells if the language was added or not.
 * 
 * 
 * 
 * Sources:
 *   datatables: https://datatables.net ( https://github.com/DataTables/DataTables )
 */


function initAdminLanguages() {

		$('#error').hide(); // Hide failed upload alert
		$('#success').hide(); // Show success alert
			
		$("#addlang-form").submit(function(e) {
        e.preventDefault();   
		
		    
		var ID= $('#languageID').val();			
		var name= $("#languageName").val();
			
		//The user wants to add a new language
		$.post("addLanguage.php", {languageID: ID, languageName: name }, function(data) {
			
			$('#error').hide();
			$('#success').hide();
					
			if(data.error== 'error') {   //It was not possible to add the new language
				
				 $('#error').show(); // show failed  alert
				 $('#LanguageCodeF').text('"' + ID + '"' );
	            
	            } 
			 
			 	if(data.success=='success') {  // Success. The language was added
			 		$("#user-table").DataTable().row.add([ID, name]).draw();
			 		$('#LanguageCodeS').text('"' + ID + '"' );
			 		$('#success').show(); // Show success alert
	            
	            }
			 
		
		},'json');
			
				
		
					
		});		

}