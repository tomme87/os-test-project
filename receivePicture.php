<?php

/**
 * ReceivePicture.php:
 *
 * Saves a subtitles's picture 
 *
 *
 */


session_start();							// Start the session
header ("Content-type: application/json");	// Vi sender svaret som json data

require_once 'include/db.php';				// Connect to the database
require_once 'classes/user.inc.php'; 		// Get user class
$user = new User($db); // Create user object.

require_once 'classes/video.inc.php'; // Get Video class.

$fn = $_FILES['file']['name'];
if (isset($_FILES['file']['name'])) {									// Dersom en fil er mottatt
  $fn = $_FILES['file']['name']; // Filnanvn.
  $video = new Video($db, $_POST['vid']); // Video object.
  $res = $video->addPicVTT($_POST['startTime'], $_POST['endTime'], $fn);

  if(isset($res['success'])) { // Successfull add.
    echo json_encode(array('ok'=>'OK', 'video' => get_object_vars($video)));
  } else {
    echo json_encode(array('error' => 'error', 'description' => $video->error));
	
  }
} else {
  echo json_encode(array('error' => 'error', 'description' => 'Filen ble ikke motatt'));
}


