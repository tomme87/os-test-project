<?php
/**
 * This class handles subtitle objects including 
 * 
 * 
 * @author tom+manuel
 */
class Subtitle {
  var $id = -1;
  var $filename = null;
  var $idVideo = null;
  var $idLanguage = null;
  private $db;


  
  const UPLOAD_DIR = 'transcripts/'; // With trailing /

  
  
  /**
   * The constructor. Get subtitle info from DB if $id set.
   *  
   * @param PDO $db database object.
   * @param int $id the subtitle ID (optional).
   * @param string $filename
   * @param int $idVideo
   * @param string $idLanguage
   */
  function Subtitle(PDO $db, $id = null, $filename = null, $idVideo = null, $idLanguage = null) {
    $this->db = $db; // Database object.
    if($id && $filename && $idVideo && $idLanguage) { // If all variables set, just add them.
      $this->id = $id;
      $this->filename = $filename;
	  $this->idVideo = $idVideo;
      $this->idLanguage = $idLanguage;
	 
    } elseif($id) { // Only ID set. get from DB
      $sql = 'SELECT id, filename, idVideo, idLanguage FROM subtitles WHERE id = ?';
      $sth = $this->db->prepare($sql);
      $sth->execute(array($id));
      if ($row = $sth->fetch(PDO::FETCH_ASSOC)) { // If subtitle exists
        $this->id = $row['id'];
        $this->filename = $row['filename'];
        $this->idVideo = $row['idVideo'];
        $this->idLanguage = $row['idLanguage'];
		
		        
      } else {
        $this->error = "Unable to get get video from ID.";
        //return false;
      }
      
    }
  }
  


  
  /**
   * Get the path to the file.
   *
   * @return string
   */
  function getPath() {
    return self::UPLOAD_DIR . $this->filename;
  }
  
  
  
   /**
   * Returnener the language of a subtitle
   * @param string $idLanguage the language code.
   * @return string with the language 
   */
  function getLanguage(){
	
	 
	  $sql = 'SELECT language FROM languages WHERE id LIKE  ?';
	  $sth = $this->db->prepare($sql);
	  $sth->execute(array($this->idLanguage));
	
	  $results = array();
	  $res = $sth->fetch(PDO::FETCH_ASSOC);
	 
       
      $resLanguage=$res['language'];
	 
	  return  $resLanguage; 
    
 
  }

  
  /**
   * Add a new Subtitle to the database.
   *
   * @param string $filename the subtitle filename.
   * @param int $idVideo the id of the Video.
   * @param string $idLanguage the language code of the subtitle.  
   * @param boolean $nosave if true, we will not atempt to save the file to disk
   * @return array that indicates failure or success.
   */
  function addSubtitle($filename, $idVideo, $idLanguage,$nosave = false) {
    $sql = "INSERT INTO subtitles (filename, idVideo, idLanguage) VALUES (?, ?, ?)";
    $sth = $this->db->prepare($sql);
    $sth->execute(array($filename, $idVideo, $idLanguage));
    if ($sth->rowCount()==0) { // Unable to add subtitle
      $this->error = "Unable to add subtitle. File probably exists, or a file with the same name exists.";
      return array('error' => 'error', 'description' => $this->error);
    }
    $this->id = $this->db->lastInsertId(); 
    $this->filename = $filename;
    $this->idVideo = $idVideo;
    $this->idLanguage = $idLanguage;

    
    if(isset($_FILES['file'])) { // Probably from a file upload. save the file to disk.
      $path = $this->getPath(); // Where to save the file.
      if(!file_exists($path)) { // Filen eksiterer ikke
        file_put_contents(						// Ta i mot filen og lagre den i transcripts katalogen
          $path,					// Her må en ta høyde for dupliserte filnavn	
          file_get_contents($_FILES['file']['tmp_name'])
        );
      } else { // Filen ekisterer fra før. (Burde som regel bli fanget opp ved innleggelse i database)
		$this->error = "Info added to DB, but file did not get saved.";
        return array('error' => 'error', 'description' => $this->error);
      }
    }
       
    //if(!isset($res['success'])) return $res;
    return(array('success'=>'success'));
	
}
  
  /**
   * Searches for subtitles for a video
   * 
   * @param string $search The search string.
   * @return array with Subtitles objects. empty array when nothing found.
   */
  function findSubtitles($search) {
    $sql = 'SELECT id, filename, idVideo, idLanguage FROM subtitles WHERE idVideo = ?';
    $sth = $this->db->prepare($sql);
    $sth-> execute(array($search));
    $results = array();

    if ($sth->rowCount()==0) { // No results
	
      return $results;
    }
    while($res = $sth->fetch(PDO::FETCH_ASSOC)) {
		
	  	
      $results[] = new Subtitle($this->db, $res['id'], $res['filename'], $res['idVideo'], $res['idLanguage']);
    }
    
    return $results;
  }
  

  

}