<?php
/**
 * Function that prints the default JS files to include on full page load.
 */
function printDefaultJS() {
  ?>
    <!-- Latest minified jQuery -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"
      integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" 
      integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    
    <!-- DataTables Table plug-in for jQuery https://datatables.net/ -->
    <script src="js/jquery.dataTables.js"></script>
    <script src="js/dataTables.bootstrap.min.js"></script>
    
    <!-- VideoJS Player framework http://videojs.com/ -->
    <script src="js/video.min.js"></script>
    <script src="js/videojs-transcript.min.js"></script>
    <script src="js/videojs-markers.min.js"></script>
    
    <!-- bootstrap-fileinput http://plugins.krajee.com/file-input -->
    <script src="js/fileinput.min.js"></script>
    
    <!-- jQuery pjax https://github.com/olemoign/jquery-pjax -->
    <script src="js/jquery.pjax.js"></script>
    
    <!-- -->
    <script src="js/jquery.sortable.min.js"></script>
    
    <!-- Egene filer for de forskjellige sidene (Vurder å legg alt dette i 1 fil.) -->
    <script src="js/login-register.js"></script>
    <script src="js/player.js"></script>
    <script src="js/upload.js"></script>
    <script src="js/uploadSubtitles.js"></script>
    <script src="js/users.js"></script>
    <script src="js/subtitleEditor.js"></script>
    <script src="js/adminLanguages.js"></script>
    <script src="js/editPlaylist.js"></script>
    
    <!-- Denne må nederst. -->
    <script src="js/main.js"></script>
    
  <?php
}

/**
 * Function that prints inline JS.
 * 
 * @param Array $jsIncInline inline scripts to run.
 */
function printInlineJS($jsIncInline = false) {
  if($jsIncInline) { // Legg til inline JS
    foreach($jsIncInline as $j) {
      echo '<script type="text/javascript">'.$j.'</script>';
    }
  }
}

/**
 * Function that prints JS files to load.
 *
 * @param Array $jsInc files to include
 */
function printJS($jsInc = false) {
  if($jsInc) { // Legg til .js
    foreach($jsInc as $j) {
      echo "<script src=\"js/$j\"></script>\n";
    }
  }
}

function endDiv() {
  echo '</div>';
}

if(!$ajax) {
  endDiv();
  printDefaultJS();
  if (isset($jsInc)) printJS($jsInc);
  if (isset($jsIncInline)) printInlineJS($jsIncInline);
} else {
  if (isset($jsInc)) printJS($jsInc); // Kanskje fjærne.
  if (isset($jsIncInline)) printInlineJS($jsIncInline);
  ?>
  <script>
    document.title = '<?php echo $pageTitle; ?>';
  </script>
  <?php
}




if(!$ajax) {
?>  
  </body>
</html>
<?php
}
?>