<?php
/**
 * Denne filen brukes for � vise en kort beskjed p� 1 side. f.eks "Du m� v�re logget inn"
 */
?>
<div class="container">
<div class="jumbotron">
  <h1><?php echo $msg['title']; ?></h1>
  <p><?php echo $msg['text']; ?></p>
  <p><a class="btn btn-primary btn-lg" href="javascript:history.back()" role="button">Back</a></p>
</div>
</div>

<?php
require_once 'include/footer.php';
//exit;
